var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletas');
var request = require('request');
var server = require('../../bin/www')

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connection to test database');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () => {
        it('status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('status 201', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers,
                url: base_url + 'create',
                body: aBici
            }, function (err, resp, body) {
                expect(resp.statusCode).toBe(201);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.ubicacion).toBe(-34);
                expect(bici.ubicacion).toBe(-54);
                done();
            });
        })
    })
});


/* afterEach(function () { console.log("testeando…"); });

describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'Rojo', 'Urbana', [6.243528, -75.570324]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function (err, resp, body) {
                expect(resp.statusCode).toBe(201);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });

});*/