const nodemailer = require('nodemailer');

const mailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'arcangel_021@outlook.com',
        pass: '12345678'
    }
};

module.exports = nodemailer.createTransport(mailConfig);